
# Mullvad Dbus

## Presentation

This project aims to provides a Dbus interface for [Mullvad](https://mullvad.net/).

It currently handles:

 * Informations availables in https://am.i.mullvad.net/json
 * DNS leak test as performed by https://am.i.mullvad.net/json
 * Account information available in [the Mullvad application](https://github.com/mullvad/mullvadvpn-app).
 * OpenVPN relay list available in [the Mullvad application](https://github.com/mullvad/mullvadvpn-app).

## Usage

### Prerequisites

You will need:

 * A modern C/C++ compiler
 * CMake 3.1+ installed

### Building The Project

```shell_session
$ git clone https://gitlab.com/mydatakeeper/apps/mullvad-dbus.git
$ cd mullvad-dbus
$ mkdir build
$ cd build
$ cmake ..
$ make -j8
```

### Installing the project

```shell_session
# make install
```

## Project Structure

There are three folders: `src`, `include`, and `dbus-desc`. Each folder serves a self-explanatory purpose.

Source files are in `src`. Header files are in `include`. Dbus description files in XML format are in `dbus-desc`.

## Contributing

**Merge Requests are WELCOME!** Please submit any fixes or improvements, and I promise to review it as soon as I can at the project URL:

 * [Project Gitlab Home](https://gitlab.com/mydatakeeper/apps/mullvad-dbus)
 * [Submit Issues](https://gitlab.com/mydatakeeper/apps/mullvad-dbus/-/issues)
 * [Merge Requests](https://gitlab.com/mydatakeeper/apps/mullvad-dbus/-/merge_requests)

## License

&copy; 2019-2020 Mydatakeeper S.A.S.

Open sourced under GPLv3 license. See attached LICENSE file.
