#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <json/json.hh>
#include <string>
#include <vector>

#include <mydatakeeper/download.h>

std::string random_id(size_t len);

JSON::Value json_rpc_request(
    const std::string& url,
    const std::string& method,
    const std::vector<std::string>& parameters);


#endif /* UTILS_H_INCLUDED */
