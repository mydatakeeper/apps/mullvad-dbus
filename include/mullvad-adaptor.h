#ifndef MULLVAD_ADAPTOR_H_INCLUDED
#define MULLVAD_ADAPTOR_H_INCLUDED

#include <mydatakeeper/dbus.h>

#include "net.mullvad.h"

extern const string MullvadPath;

class MullvadAdaptor
: public DBusAdaptor,
  public net::mullvad_adaptor
{
public:
    using ports_t = vector<uint16_t>;
    using relay_t = DBus::Struct<string, string, uint32_t, ports_t, ports_t>;
    using location_t = DBus::Struct<string, string, string, double, double, vector<relay_t>>;
    using dns_t = DBus::Struct<string, string, bool, string>;

    MullvadAdaptor(DBus::Connection &connection);
    virtual ~MullvadAdaptor() {}

    virtual bool set_account(const std::string& account_number);
    virtual bool delete_account();

    virtual void add_voucher(const std::string& voucher);
    virtual std::string apply_voucher(const std::string& voucher);
    virtual bool apply_vouchers();

    virtual bool add_port();
    virtual bool remove_port(const uint16_t& port);

    virtual location_t get_closest_location();

    bool update_connection_values();
    bool update_dns_values();
    bool update_relays();
    bool update_expiry_date();
    bool update_account_values();

    void properties_changed(const std::vector<std::string>& properties);

private:
    void add_voucher(const std::string& voucher, const string& status);
    std::string apply_voucher(const std::string& voucher, bool trigger_signal);
};

#endif /* MULLVAD_ADAPTOR_H_INCLUDED */
