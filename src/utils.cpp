#include "utils.h"

using namespace std;

string random_id(size_t len)
{
    static const string alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    string str(len, '\0');
    srand(time(nullptr));
    for (auto& c : str)
        c = alphanum[rand() % alphanum.size()];
    return str;
}

JSON::Value json_rpc_request(
    const string& url,
    const string& method,
    const vector<string>& parameters)
{
    const string http_method("POST");
    const vector<string> http_headers = {"Content-Type: application/json"};

    string id = random_id(12);
    string params;
    for (auto& str : parameters) {
        if (!params.empty())
            params += ',';
        params += "\"" + str + "\"";
    }
    string data = "{"
        "\"jsonrpc\":\"2.0\","
        "\"id\":\"" + id + "\","
        "\"method\":\"" + method + "\","
        "\"params\":[" + params + "]"
    "}";
    string buffer, err;
    if (!download_file(url, http_method, http_headers, data, buffer, &err)) {
        cerr << err << endl;
        return {};
    }

    JSON::Object json;
    try {
        json = parse_string(buffer);
    } catch (exception& e) {
        cerr << e.what() << endl;
        return {};
    }

    if (json["jsonrpc"].as_string() != "2.0") {
        cerr << "Bad JsonRPC version (expected: 2.0, actual: " << json["jsonrpc"].as_string() << ')' << endl;
        return {};
    }

    if (json["id"].as_string() != id) {
        cerr << "Wrong request ID returned (expected: " << id << ", actual: " << json["id"].as_string() << ')' << endl;
        return {};
    }

    if (json["error"].type() != JSON::NIL) {
        JSON::Object error = json["error"];
        cerr << error["message"] << "(code: " << error["code"] << ')' << endl;
        return {};
    }

    return json["result"];
}