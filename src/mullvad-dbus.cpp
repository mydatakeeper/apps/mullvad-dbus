#include <atomic>
#include <chrono>
#include <condition_variable>
#include <csignal>
#include <cstring>
#include <iostream>
#include <mutex>
#include <thread>

#include <dbus-c++/dbus.h>

#include <mydatakeeper/arguments.h>

#include <mydatakeeper/config.h>
#include <mydatakeeper/applications.h>

#include "mullvad-adaptor.h"

#ifndef VERSION
#define VERSION "debug"
#endif

const string MullvadNet("net.mullvad");

using namespace std;

DBus::BusDispatcher dispatcher;

condition_variable update_cond;
atomic<bool> update_exit;

void update(auto& mullvad)
{
    while (!update_exit.load()) {
        // Waiting for next update
        uint64_t now = chrono::duration_cast<chrono::seconds>(
            chrono::system_clock::now().time_since_epoch()).count();
        int32_t timeout = mullvad.LastUpdateSuccessful() ?
            mullvad.UpdateTimeout() : mullvad.RetryTimeout();
        uint64_t next_update = mullvad.LastUpdate() + timeout;
        if (now < next_update) {
            uint32_t sleep = next_update - now;
            clog << "Next update in " << sleep << " seconds" << endl;
            mutex mtx;
            unique_lock<mutex> lck(mtx);
            update_cond.wait_for(lck, chrono::seconds(sleep));
            continue;
        }

        clog << "Updating Mullvad values" << endl;
        mullvad.LastUpdate = now;
        bool last_update_successful = true;
        mutex mtx;
        vector<thread> threads;
        threads.emplace_back([&last_update_successful, &mtx, &mullvad]() {
            bool ret = mullvad.update_connection_values();
            unique_lock<mutex> lck(mtx);
            last_update_successful &= ret;
        });
        threads.emplace_back([&last_update_successful, &mtx, &mullvad]() {
            bool ret = mullvad.update_dns_values();
            unique_lock<mutex> lck(mtx);
            last_update_successful &= ret;
        });
        threads.emplace_back([&last_update_successful, &mtx, &mullvad]() {
            bool ret = mullvad.update_relays();
            unique_lock<mutex> lck(mtx);
            last_update_successful &= ret;
        });
        threads.emplace_back([&last_update_successful, &mtx, &mullvad]() {
            bool ret = mullvad.update_expiry_date();
            unique_lock<mutex> lck(mtx);
            last_update_successful &= ret;
        });
        threads.emplace_back([&last_update_successful, &mtx, &mullvad]() {
            bool ret = mullvad.update_account_values();
            unique_lock<mutex> lck(mtx);
            last_update_successful &= ret;
        });
        for (auto& t : threads)
            t.join();
        mullvad.LastUpdateSuccessful = last_update_successful;

        mullvad.properties_changed({"LastUpdate", "LastUpdateSuccessful"});
    }
    clog << "Exiting Mullvad update thread" << endl;
}

void bus_type(const string& name, const string& value)
{
    if (value != "system" && value != "session")
        throw invalid_argument(name + "must be either 'system' or 'session'");
}

void signal_handler(int sig)
{
    clog << strsignal(sig) << " signal received" << endl;
    dispatcher.leave();
}

int main(int argc, char** argv)
{
    string bus, bus_name, bus_path, account_bus_name, account_bus_path;
    mydatakeeper::parse_arguments(argc, argv, VERSION, {
        {
            bus, "bus", 0, mydatakeeper::required_argument, "system", bus_type,
            "The type of bus used for Dbus object. Either system or session"
        },
        {
            bus_name, "bus-name", 0, mydatakeeper::required_argument, MullvadNet, nullptr,
            "The Dbus name of the Mullvad"
        },
        {
            bus_path, "bus-path", 0, mydatakeeper::required_argument, MullvadPath, nullptr,
            "The Dbus path of the Mullvad"
        },
        {
            account_bus_name, "account-bus-name", 0, mydatakeeper::required_argument, fr::mydatakeeper::ApplicationsName, nullptr,
            "The Dbus name of mydatakeeper-account"
        },
        {
            account_bus_path, "account-bus-path", 0, mydatakeeper::required_argument, "/fr/mydatakeeper/account", nullptr,
            "The Dbus path of mydatakeeper-account"
        },
    });

    // Setup signal handler
    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);

    // Setup DBus dispatcher
    DBus::default_dispatcher = &dispatcher;

    // Update variables depending on parameters
    DBus::Connection conn = (bus == "system") ?
        DBus::Connection::SystemBus() :
        DBus::Connection::SessionBus();

    clog << "Setting up Mullvad proxy" << endl;
    MullvadAdaptor mullvad(conn);
    mullvad.RetryTimeout = 60;
    mullvad.UpdateTimeout = 60*60*3;
    mullvad.LastUpdate = 0;
    mullvad.LastUpdateSuccessful = false;

    mullvad.onSet = [&mullvad](auto& /*iface*/, auto& name, auto& value) {
        if (name == "RetryTimeout") {
            mullvad.RetryTimeout = value.operator uint32_t();
            if (!mullvad.LastUpdateSuccessful())
                update_cond.notify_one();
            mullvad.properties_changed({name});
        }
        if (name == "UpdateTimeout") {
            mullvad.UpdateTimeout = value.operator uint32_t();
            if (mullvad.LastUpdateSuccessful())
                update_cond.notify_one();
            mullvad.properties_changed({name});
        }
    };

    clog << "Getting Config " << bus_path << " proxy" << endl;
    fr::mydatakeeper::ConfigProxy account(conn, account_bus_path, account_bus_name);

    clog << "Setting up Config " << bus_path << " update listener" << endl;
    account.onPropertiesChanged = [&mullvad](auto &/*iface*/, auto &changed, auto &/*invalidated*/)
    {
        if (changed.find("metadata") != changed.end()) {
            clog << "Account metadata has changed" << endl;
            mullvad.delete_account();
            vector<DBus::Struct<string, string>> metadata = changed.at("metadata");

            // Setup an account first
            for (auto& it : metadata) {
                if (it._1 == "mullvad_account") {
                    clog << "Setting up Mullvad account" << endl;
                    mullvad.set_account(it._2);
                }
            }

            // The check for vouchers
            for (auto& it : metadata) {
                if (it._1 == "mullvad_voucher") {
                    clog << "Applying Mullvad voucher" << endl;
                    mullvad.apply_voucher(it._2);
                }
            }
        }
    };

    clog << "Starting update thread" << endl;
    thread update_thread([&mullvad]() { update(mullvad); });

    clog << "Requesting DBus name" << endl;
    conn.request_name(bus_name.c_str());

    clog << "Starting DBus main loop" << endl;
    dispatcher.enter();

    clog << "Waiting update thread to exit" << endl;
    update_exit.store(true);
    update_cond.notify_one();
    update_thread.join();

    return 0;
}