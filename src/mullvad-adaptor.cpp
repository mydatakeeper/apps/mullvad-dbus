#include "mullvad-adaptor.h"

#include <cmath>

#include "utils.h"

const string MullvadUrl("https://mullvad.net");
const string MullvadApiUrl("https://api.mullvad.net");
const string AmIMullvadUrl("https://am.i.mullvad.net");

namespace {

template <typename T>
inline DBus::Variant to_new_variant(const DBus::Variant& v1, const T t2)
{
    T t1 = v1.operator T();
    if (t1 != t2)
        return to_variant(t2);
    return {};
}

DBus::Variant to_new_variant(const DBus::Variant& v1, const JSON::Value& v2)
{
    auto sig = v1.signature();
    auto type = v2.type();
    if (sig == "b" && type == JSON::BOOL)
        return to_new_variant<bool>(v1, v2.as_bool());
    if (sig == "n" && type == JSON::INT)
        return to_new_variant<int16_t>(v1, v2.as_int());
    if (sig == "q" && type == JSON::INT)
        return to_new_variant<uint16_t>(v1, v2.as_int());
    if (sig == "i" && type == JSON::INT)
        return to_new_variant<int32_t>(v1, v2.as_int());
    if (sig == "u" && type == JSON::INT)
        return to_new_variant<uint32_t>(v1, v2.as_int());
    if (sig == "x" && type == JSON::INT)
        return to_new_variant<int64_t>(v1, v2.as_int());
    if (sig == "t" && type == JSON::INT)
        return to_new_variant<uint64_t>(v1, v2.as_int());
    if (sig == "d" && type == JSON::FLOAT)
        return to_new_variant<double>(v1, v2.as_float());
    if (sig == "s" && type == JSON::STRING)
        return to_new_variant<string>(v1, v2.as_string());
    return {};
}

bool has_key(const JSON::Object& json, const string &key)
{
    auto search = find_if(json.begin(), json.end(), [&key](const auto& it) {
        return it.first == key;
    });
    return search != json.end();
}

double haversine(double lat1, double lon1,
                 double lat2, double lon2)
{
    // distance between latitudes and longitudes
    double dLat = (lat2 - lat1) * M_PI / 180.0;
    double dLon = (lon2 - lon1) * M_PI / 180.0;

    // convert to radians
    lat1 = (lat1) * M_PI / 180.0;
    lat2 = (lat2) * M_PI / 180.0;

    // apply formulae
    double a =
        pow(sin(dLat / 2), 2) +
        pow(sin(dLon / 2), 2) *
        cos(lat1) * cos(lat2);
    double rad = 6371;
    double c = 2 * asin(sqrt(a));
    return rad * c;
}

} /* end namespace */

extern const string MullvadPath("/net/mullvad");

MullvadAdaptor::MullvadAdaptor(DBus::Connection &connection)
: DBusAdaptor(connection, MullvadPath)
{
    this->city = string();
    this->country = string();
    this->ip = string();
    this->latitude = 0.0;
    this->longitude = 0.0;
    this->mullvad_exit_ip = false;
    this->organization = string();
    this->dns = vector<dns_t>();
    this->dns_leak = true;
    this->account_number = string();
    this->expiry_date = 0;
    this->vouchers = map<string, string>();
    this->ports = vector<uint16_t>();
    this->max_ports = 0;
    this->assigned_ports = 0;
    this->openvpn_relays = vector<location_t>();
}

void MullvadAdaptor::properties_changed(const vector<string>& properties)
{
    map<string, DBus::Variant> changed;
    for(auto& prop : properties)
        changed[prop] = *net::mullvad_adaptor::get_property(prop);
    PropertiesChanged(net::mullvad_adaptor::name(), changed, {});
}

bool MullvadAdaptor::set_account(const string& account_number)
{
    const string old_account_number = this->account_number();
    this->account_number = account_number;

    if (update_expiry_date() && update_account_values()) {
        properties_changed({"account_number"});
        return true;
    }

    cerr << "Bad account number" << endl;
    this->account_number = old_account_number;

    update_expiry_date() && update_account_values();

    return false;
}

bool MullvadAdaptor::delete_account()
{
    this->account_number = "";
    this->expiry_date = (uint64_t)0;
    this->vouchers = map<string, string>();
    this->ports = vector<uint16_t>();
    this->max_ports = (uint16_t)0;
    this->assigned_ports = (uint16_t)0;

    properties_changed({
        "account_number",
        "expiry_date",
        "vouchers",
        "ports",
        "max_ports",
        "assigned_ports"
    });

    return true;
}

void MullvadAdaptor::add_voucher(const string& voucher)
{
    add_voucher(voucher, "PENDING");
}

void MullvadAdaptor::add_voucher(const string& voucher, const string& status)
{
    auto vouchers = this->vouchers();
    vouchers[voucher] = status;
    this->vouchers = vouchers;
}

string MullvadAdaptor::apply_voucher(const string& voucher)
{
    return apply_voucher(voucher, true);
}

string MullvadAdaptor::apply_voucher(const string& voucher, bool trigger_signal)
{
    string buffer, err;
    string data;
    vector<string> headers;
    JSON::Object json;

    string status = "PENDING";
    const auto account_number = this->account_number();
    if (account_number.empty()) {
        status = "NO_ACCOUNT";
        goto end;
    }

    static const string url = MullvadApiUrl + "/www/payments/submit-voucher/";
    static const string method("POST");
    data = "voucher_code=" + voucher;
    headers = {"Authorization: Token " + account_number};
    if (!download_file(url, method, headers, data, buffer, &err)) {
        cerr << err << endl;
        status = "BAD_REQUEST";
        goto end;
    }

    try {
        json = parse_string(buffer);
    } catch (exception& e) {
        cerr << e.what() << endl;
        status = "BAD_RESPONSE";
        goto end;
    }

    status = json["code"].as_string();
    if (status == "INVALID_VOUCHER") {
        cerr << "Invalid voucher" << endl;
    }

end:
    add_voucher(voucher, status);
    if (trigger_signal)
        properties_changed({"vouchers"});

    return status;
}

bool MullvadAdaptor::apply_vouchers()
{
    const auto account_number = this->account_number();
    if (account_number.empty())
        return false;

    auto success = true;
    const auto vouchers = this->vouchers();
    for (auto& it : vouchers) {
        auto status = apply_voucher(it.first, false);
        if (status == "BAD_REQUEST" || status == "BAD_RESPONSE")
            success = false;
    }

    properties_changed({"vouchers"});
    return success;
}

bool MullvadAdaptor::add_port()
{
    const auto account_number = this->account_number();
    if (account_number.empty())
        return false;

    string buffer, err;
    const string url = MullvadApiUrl + "/accounts/" + account_number + "/add_port/";
    static const string method("POST");
    if (!download_file(url, method, buffer, &err)) {
        cerr << err << endl;
        return false;
    }

    return update_account_values();
}

bool MullvadAdaptor::remove_port(const uint16_t& port)
{
    const auto account_number = this->account_number();
    if (account_number.empty())
        return false;

    string buffer, err;
    const string url = MullvadApiUrl + "/accounts/" + account_number + "/remove_port/";
    static const string method("POST");
    const string data = "port=" + to_string(port);
    if (!download_file(url, method, {}, data, buffer, &err)) {
        cerr << err << endl;
        return false;
    }

    return update_account_values();
}

MullvadAdaptor::location_t MullvadAdaptor::get_closest_location()
{
    const auto locations = this->openvpn_relays();
    const auto lat = this->latitude();
    const auto lon = this->longitude();
    const auto min = min_element(locations.begin(), locations.end(),
        [lat, lon](auto &l1, auto &l2) {
            return
                haversine(lat, lon, l1._4, l1._5)
                <
                haversine(lat, lon, l2._4, l2._5);
        }
    );

    if (min == locations.end())
        throw DBus::ErrorNoReply("No relay available");

    return *min;
}


bool MullvadAdaptor::update_connection_values()
{
    string buffer, err;
    if (!download_file(AmIMullvadUrl + "/json", buffer, &err)) {
        cerr << err << endl;
        return false;
    }

    const vector<string> defined_values = {
        "city", "country", "ip",
        "latitude", "longitude", "mullvad_exit_ip", "organization",
    };

    JSON::Object json;
    try {
        json = parse_string(buffer);
    } catch (exception& e) {
        cerr << e.what() << endl;
        return false;
    }

    map<string, DBus::Variant> updated_values;
    for (auto& it : json) {
        auto search = find(defined_values.begin(), defined_values.end(), it.first);
        if (search == defined_values.end())
            continue;
        DBus::Variant *old_value = net::mullvad_adaptor::get_property(it.first);
        DBus::Variant new_value = to_new_variant(*old_value, it.second);
        if (!new_value.signature().empty()) {
            clog << "Updating value \"" << it.first << "\" to " << it.second << endl;
            auto& property = net::mullvad_adaptor::_properties[it.first].value;
            property = new_value;
            updated_values[it.first] = new_value;
        }
    }

    PropertiesChanged(net::mullvad_adaptor::name(), updated_values, {});

    return true;
}

bool MullvadAdaptor::update_dns_values()
{
    string buffer, err;
    const vector<string> uuids = {
        random_id(32), random_id(32), random_id(32), random_id(32)
    };
    map<string, dns_t> dns_map;
    bool dns_leak = false;

    // Run several times to maximize the chances to capture all dns
    for (auto& uuid : uuids) {
        const string url = "https://" + uuid + ".dnsleak.am.i.mullvad.net";
        download_file(url, buffer, &err); // Wait for timeout/failure
    }

    // Run DNS checks after DNS requests
    for (auto& uuid : uuids) {
        const string url = AmIMullvadUrl + "/dnsleak/" + uuid;
        if (!download_file(url, buffer, &err)) {
            cerr << err << endl;
            return false;
        }

        JSON::Array json;
        try {
            json = parse_string(buffer);
        } catch (exception& e) {
            cerr << e.what() << endl;
            return false;
        }

        for (const JSON::Object& dns_info : json) {
            string country = dns_info["country"].as_string();
            string ip = dns_info["ip"].as_string();
            bool mullvad_dns = dns_info["mullvad_dns"].as_bool();
            string organization = has_key(dns_info, "organization") ?
                dns_info["organization"].as_string() : string();

            dns_leak |= !mullvad_dns;
            dns_map[ip] = {country, ip, mullvad_dns, organization};
        }
    }

    // We assume there might be some dns leak if we cannot gather information
    if (dns_map.size() == 0)
        dns_leak = true;

    vector<dns_t> all_dns;
    for (const auto& it : dns_map) all_dns.push_back(it.second);

    clog << "Updating value \"dns\" and \"dns_leak\"" << endl;
    this->dns = all_dns;
    this->dns_leak = dns_leak;

    properties_changed({"dns", "dns_leak"});

    return true;
}

bool MullvadAdaptor::update_relays()
{
    string buffer, err;
    static const string url = MullvadApiUrl + "/rpc/";
    const JSON::Value relays = json_rpc_request(url, "relay_list_v3", {});
    if (relays.type() == JSON::NIL) {
        return false;
    }

    vector<location_t> locations;

    const JSON::Array& json_countries = relays["countries"];
    for (const JSON::Object& json_country: json_countries) {
        const string country_code = json_country["code"].as_string();
        const string country_name = json_country["name"].as_string();
        const JSON::Array& json_cities = json_country["cities"];
        for (const JSON::Object& json_city: json_cities) {
            const string city_code = json_city["code"].as_string();
            const string city_name = json_city["name"].as_string();
            const double latitude = json_city["latitude"].as_float();
            const double longitude = json_city["longitude"].as_float();

            location_t location;
            location._1 = country_code + '/' + city_code;
            location._2 = country_name;
            location._3 = city_name;
            location._4 = latitude;
            location._5 = longitude;

            const JSON::Array& json_relays = json_city["relays"];
            for (const JSON::Object& json_relay: json_relays) {
                const bool active = json_relay["active"].as_bool();
                if (!active)
                    continue;

                const string hostname = json_relay["hostname"].as_string();
                const string ipv4 = json_relay["ipv4_addr_in"].as_string();
                const uint32_t weight = json_relay["weight"].as_int();

                if (!has_key(json_relay, "tunnels"))
                    continue;

                const JSON::Object& json_tunnels = json_relay["tunnels"];
                if (!has_key(json_tunnels, "openvpn"))
                    continue;

                relay_t relay;
                relay._1 = hostname;
                relay._2 = ipv4;
                relay._3 = weight;

                const JSON::Array& json_openvpn = json_tunnels["openvpn"];
                for (const JSON::Object& openvpn: json_openvpn) {
                    const string protocol = openvpn["protocol"].as_string();
                    const uint16_t port = openvpn["port"].as_int();
                    if (protocol == "udp") {
                        relay._4.push_back(port);
                    } else if (protocol == "tcp") {
                        relay._5.push_back(port);
                    }
                }

                if (!relay._4.empty() && !relay._5.empty()) {
                    location._6.push_back(relay);
                }
            }

            if (!location._6.empty()) {
                locations.push_back(location);
            }
        }
    }

    this->openvpn_relays = locations;

    properties_changed({"openvpn_relays"});

    return true;
}

bool MullvadAdaptor::update_expiry_date()
{
    const string account_number = this->account_number();
    if (account_number.empty()) {
        clog << "Resetting expiry_date value" << endl;
        this->expiry_date = (uint64_t)0;

        properties_changed({"expiry_date"});

        return true;
    }

    string buffer, err;
    static const string url = MullvadApiUrl + "/rpc/";
    const JSON::Value expiry = json_rpc_request(url, "get_expiry", {account_number});
    if (expiry.type() == JSON::NIL) {
        return false;
    }

    const string strtime = expiry.as_string();
    struct tm time;
    strptime(strtime.c_str(), "%Y-%m-%dT%H:%M:%S+00:00", &time);
    const time_t gmttime = timegm(&time);
    clog << "Updating value \"expiry_date\" to \"" << gmttime << '"' << endl;
    this->expiry_date = (uint64_t)gmttime;

    properties_changed({"expiry_date"});

    return true;
}

bool MullvadAdaptor::update_account_values()
{
    const string account_number = this->account_number();
    if (account_number.empty()) {
        clog << "Resetting account values" << endl;
        this->ports = vector<uint16_t>();
        this->max_ports = (uint16_t)0;
        this->assigned_ports = (uint16_t)0;

        properties_changed({"ports", "max_ports", "assigned_ports"});

        return true;
    }

    string buffer, err;
    const string url = MullvadApiUrl + "/accounts/" + account_number + "/wg_pubkeys/";
    if (!download_file(url, buffer, &err)) {
        cerr << err << endl;
        return false;
    }

    JSON::Object json;
    try {
        json = parse_string(buffer);
    } catch (exception& e) {
        cerr << e.what() << endl;
        return false;
    }

    vector<uint16_t> ports;
    const JSON::Array& json_ports = json["ports"];
    for (auto& port : json_ports)
        ports.push_back((uint16_t)port.as_int());
    clog << "Updating value \"ports\"" << endl;
    this->ports = ports;
    clog << "Updating value \"max_ports\" to " << json["max_ports"] << endl;
    uint16_t max_ports = (uint16_t)json["max_ports"].as_int();
    this->max_ports = max_ports;
    clog << "Updating value \"assigned_ports\" to " << json["unassigned_ports"] << endl;
    uint16_t assigned_ports = (uint16_t)json["unassigned_ports"].as_int();
    this->assigned_ports = assigned_ports;

    properties_changed({"ports", "max_ports", "assigned_ports"});

    return true;
}
